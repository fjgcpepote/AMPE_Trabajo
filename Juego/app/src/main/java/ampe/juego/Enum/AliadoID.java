package ampe.juego.Enum;

/**
 * Enumerado que se utilizará para saber que tipo de Aliado hay que contruir en la Fábrica
 */

public enum AliadoID
{
    GUITARRA, VIOLIN, SAXOFON
}
