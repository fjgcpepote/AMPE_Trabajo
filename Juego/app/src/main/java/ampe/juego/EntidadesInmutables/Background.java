package ampe.juego.EntidadesInmutables;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;


/**
 * Aunque cuenta como una entidad solo habrá un tipo de Fondo, por tanto es una clase única
 */
public class Background {
    //El bitmap que se dibujará en el draw
    private Bitmap imagenDeFondo;

    //x,y será 0,0 ya que es donde se inicia la imagen
    private int x,y;

    /**
     * Constructor al que hay que pasarle el bitmap ya decodificado
     * @param imagenDeFondo
     */
    public Background(Bitmap imagenDeFondo)
    {
        this.imagenDeFondo = imagenDeFondo;

    }
    public void update()
    {
        /**
         * Vacio por que imagen va a ser estática
         */
    }
    public void draw(Canvas canvas, Paint paint)
    {

      if(canvas!=null)
      {
          //Se utiliza para la eficiencia
          final int instanciaGuardada = canvas.save();
          //Se dibuja
          canvas.drawBitmap(imagenDeFondo, x,y,paint);
          //Se recupera lo anterior
          canvas.restoreToCount(instanciaGuardada);
      }


    }
}
