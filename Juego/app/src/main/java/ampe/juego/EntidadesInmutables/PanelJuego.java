package ampe.juego.EntidadesInmutables;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.constraint.solver.widgets.Rectangle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

import ampe.juego.Actividades.ActividadJuego;
import ampe.juego.Controladores.HiloCargador;
import ampe.juego.Enum.AliadoID;
import ampe.juego.EntidadesFila.Enemigo;
import ampe.juego.Fabricas.FabricaAliados;
import ampe.juego.Fabricas.FabricaEnemigos;
import ampe.juego.Fabricas.FabricaObjetosFilas;
import ampe.juego.Controladores.HiloJuegoPrincipal;

/**
 * Clase más importante de la aplicación de la que cuelgan todos los elemntos del juego
 */
//SurfaceView para que se dibuje, SurfaceHolder para los eventos (pulsar pantalla)
public class PanelJuego extends SurfaceView implements SurfaceHolder.Callback {

    /**
     * CONSTANTES
     */
    //
    public final static int NFILAS = 3;/**Numero de filas que pondremos para enmigos/aliados*/
    public static final int NALIADOS_EN_FILA = 5;/**Numero máximo de aliados que hay en una Fila*/
    public final static float PULGADAS_ANCHO_FONDO = 26.5f;/**Pulgadas en las que se divide la longitud de la imagen*/
    public final static float PULGADAS_ALTO_FONDO = 15f;/**Pulgadas en las que se divide la altura de la imagen*/
    public final static float PULGADAS_ANCHO_BOTON = 3.5f;
    public final static float PULGADAS_ALTO_BOTON = 1.8f;
    public final static float PULGADA_Y_INICIO_ZONA_ALIADOS = 8f;
    public final static float PULGADAS_ALTO_ZONA_ALIADOS = 4f;
    public final static float PULGADA_X_FIN_ZONA_ALIADOS = 15.5f;
    public final static float PULGADAS_ENTRE_FILAS = 1.33f;
    public static final float AJUSTE_TEXTO_PUNTUACION_Y = 0.046f;
    public static final float AJUSTE_TEXTO_PUNTUACION_X = 0.67f;
    public static final float AJUSTE_TEXTO_RONDA_X = 0.4f;
    public static final float AJUSTE_TEXTO_RONDA_Y = 0.2f;
    public static final int PUNTOS_GUITARRA = 150;
    public static final int PUNTOS_VIOLIN = 200;
    public static final int PUNTOS_SAXOFON = 400;
    /**
     *
     */
    /**
     * VARIABLES GLOBALES
     */

    public static HiloJuegoPrincipal HILO_JUEGO;  //El hilo que va a decidir cuando hacer update y draw
    public static boolean COLOCANDO_ALIADO = false;//Para saber si se está intentando colocar un nuevo aliado
    public static FabricaEnemigos FABRICA_ENEMIGOS; /** Para crear las entidades de las filas de forma cómoda*/
    public static FabricaObjetosFilas FABRICA_OBJETOS_FILAS;
    public static FabricaAliados FABRICA_ALIADOS;
    public static int ENEMIGOS_RESTANTES_RONDA;
    public static PanelJuego UNICAINSTANCIA;

    /**
     * CAMPOS LOCALES
     */
    private Paint paint;//Para añadir texto y un pintado? común

    //El ID que tendrá el Aliado que se pretende añadir
    private AliadoID aliadoID;
    //Imagen que estará de fondo en el Juego
    private Background fondo;
    //Puntos utilizados para poner nuevos aliados
    private int puntuacion = 350;
    //Los puntos que restará un nuevo aliado al ser colocado
    private int restaPuntos;
    //La ronda por la que vamos en el juego
    private int ronda = 0;
    //Los enemigos que quedan por matar para cambiar de ronda

    //Variable para saber cuantos enemigos aún no se hhan creado e introducido en esta ronda
    private int enemigosPorMeter;
    //random
    private Random rand = new Random();


    //Filas que hay en el PanelJuego
    private Fila[] filas;
    //Botones para añadir los aliados
    private Rectangle botonGuitarra, botonViolin, botonSaxofon;
    //Una zona utilizada para saber si al intentar colocar un nuevo aliado hemos tocado en zona
    //correcta
    private Rectangle zonaAliados;

    //Utilizado para saber si ha pasado el tiempo necesario entre que se introducen enemigos
    private long ultimoTiempoMetidaEnemigo;
    //El tiempo que se quiere dejar entre la aparición de enemigos
    private float tiempoEntreEnemigos = 1.5f;
    private boolean datosCargados = false;
    private Thread hiloCargador;
    /**
     * Programación con hilos eficiencia
     */

    public void cargarDatos()
    {
        if(!datosCargados) {
            hiloCargador = new HiloCargador(this);
            hiloCargador.start();
        }

    }
    public void esperarDatos()
    {

        try {
            hiloCargador.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        datosCargados = true;
    }

    /**
     * Panel Juego es un Singleton
     */
    public static PanelJuego getUnicaInstancia(Context contexto)
    {
        if(UNICAINSTANCIA==null)
            UNICAINSTANCIA = new PanelJuego(contexto);
        return UNICAINSTANCIA;
    }
    /**
     * Constructor de un SurfaceView al que se le pasa el contexto
     */
    private PanelJuego(Context context) {
        super(context);
        //Para detectar los eventos que se producen
        this.getHolder().addCallback(this);
        //Focusable para la gestión de eventos
        setFocusable(true);
        paint = new Paint();//Pintado del canvas
        paint.setColor(Color.RED);
        paint.setTextSize(45);



    }
    //Se hace override para que no lance una excepción
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        /**
         * Falta por implementar
         */
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean reintentar = true;
        int intentos = 0;
        while(reintentar && intentos < 1000)
        {
            intentos++;
            try{
                HILO_JUEGO.finalizarJuego();
                HILO_JUEGO.join();
                reintentar = false;
            }
            catch(InterruptedException e){
                e.printStackTrace();

            }

        }
    }



    /**
     *
     * Cuando se inicia la actividad vendrá aquí directamente, este es el punto de entrada de la aplicación
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        HILO_JUEGO = new HiloJuegoPrincipal(getHolder(),this, paint);

         /** Empieza la aplicación
         */
        HILO_JUEGO.start();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        super.onTouchEvent(event);
        /**
         * Si se pulsa la pantalla y no se está colocando un aliado, se mira a ver donde se ha pulsado
         */
        if (event.getAction() == MotionEvent.ACTION_DOWN && !COLOCANDO_ALIADO) {
            int xPulsada = (int) event.getX();
            int yPulsada = (int) event.getY();
            /**Si hemos pulsado en algún botón y tenemos los puntos suficientes,
             *  activamos el parámetro indicando que queremos
            *poner un nuevo aliado
             */
            if (botonGuitarra.contains(xPulsada, yPulsada) && puntuacion >= PUNTOS_GUITARRA) {
                COLOCANDO_ALIADO = true;
                aliadoID = AliadoID.GUITARRA;
                restaPuntos = PUNTOS_GUITARRA;
            }
            else if (botonViolin.contains(xPulsada, yPulsada) && puntuacion >= PUNTOS_VIOLIN){
                COLOCANDO_ALIADO = true;
                aliadoID = AliadoID.VIOLIN;
                restaPuntos= PUNTOS_VIOLIN;
            }
            else if (botonSaxofon.contains(xPulsada, yPulsada) && puntuacion >= PUNTOS_SAXOFON){
                COLOCANDO_ALIADO = true;
                aliadoID = AliadoID.SAXOFON;
                restaPuntos = PUNTOS_SAXOFON;
            }
        }
        /**
         * Si ya estabamos intentado poner un aliado nuevo, miramos a ver si seleccionamos
         * una zona en la que se puedan poner los aliados y luego miramos si no hay ninguno
         * ya puesto
         */
        else if (event.getAction() == MotionEvent.ACTION_DOWN && COLOCANDO_ALIADO)
        {
            int xPulsada = (int) event.getX();
            int yPulsada = (int) event.getY();
            int filaSleccionada = -1;

            //Miramos si hemos metido alguna Fila correcta
            for(int i = 0; i<NFILAS && filaSleccionada==-1; i++)
                if(yPulsada >= ((PULGADA_Y_INICIO_ZONA_ALIADOS+PULGADAS_ENTRE_FILAS*i)*getHeight())/PULGADAS_ALTO_FONDO && (yPulsada < ((PULGADA_Y_INICIO_ZONA_ALIADOS+PULGADAS_ENTRE_FILAS*(i+1))*getHeight())/PULGADAS_ALTO_FONDO) )
                    filaSleccionada =i;

            if(filaSleccionada!=-1 )
            {
                //Si tenemos la fila correcta miramos a ver si la X es correcta
                int posicionX = -1;
                for(int i=0;i<5 && posicionX == -1;i++) {
                    if ((xPulsada >= (((PULGADA_X_FIN_ZONA_ALIADOS/NALIADOS_EN_FILA)*i) * getWidth()) / PULGADAS_ANCHO_FONDO) && (xPulsada < (((PULGADA_X_FIN_ZONA_ALIADOS/NALIADOS_EN_FILA)*(i+1)) * getWidth()) / PULGADAS_ANCHO_FONDO))
                       posicionX = i+1 ;
                }
                //Si la X es correcta miramos a ver si hay un hueco libre, y en ese caso añadimos al aliado
                //Y descontamos los puntos
                 if(posicionX != -1 && !filas[filaSleccionada].isPosicionOcupada(posicionX)) {
                     filas[filaSleccionada].addAliado(FABRICA_ALIADOS.crearAliadoID(aliadoID), posicionX);
                     puntuacion-=restaPuntos;
                 }
            }

            COLOCANDO_ALIADO = false;
        }
       return true;
    }
    public void update()
    {
        if(ENEMIGOS_RESTANTES_RONDA <= 0) {
            ronda++;
            puntuacion+=100;
            //Si cambiamos de ronda, aumentamos en uno el número de enemigos con respecto
            //a la anterior
            ENEMIGOS_RESTANTES_RONDA = ronda + 3;
            //Cda vez los enemigos salen más rápido
            tiempoEntreEnemigos-=0.08f;
            //Para saber cuantos faltan por añadir
            enemigosPorMeter =  ENEMIGOS_RESTANTES_RONDA;

        }
        //Para saber si hha pasado el tiempo entre meter y meter enemigos
        long tiempoActual = System.nanoTime();

        long tiempoTranscurrido = (long) ((tiempoActual - ultimoTiempoMetidaEnemigo)/1000000000.0);
        if(enemigosPorMeter> 0 && (tiempoTranscurrido > tiempoEntreEnemigos))
        {
            int  n = rand.nextInt(NFILAS);
            Enemigo e = FABRICA_ENEMIGOS.creaEnemigo();
            //Los enemigos son más rápidos a cada ronda que pasa
            e.aumentarAceleracion(ronda);
            filas[n].addEnemigo(e);
            enemigosPorMeter--;
            ultimoTiempoMetidaEnemigo = System.nanoTime();
        }

        fondo.update();
        for(Fila f : filas)
            if(f!=null)
            f.update();



    }
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        fondo.draw(canvas, paint);
        for(Fila f : filas)
            if(f!=null)
                f.draw(canvas, paint);
        canvas.drawText("Puntos: " + Integer.toString(puntuacion), (float) (getWidth() * AJUSTE_TEXTO_PUNTUACION_X), getHeight()*AJUSTE_TEXTO_PUNTUACION_Y, paint);
        paint.setColor(Color.BLUE);
        paint.setTextSize(60);
        canvas.drawText("Ronda: " + Integer.toString(ronda),(getWidth() * AJUSTE_TEXTO_RONDA_X), getHeight() * AJUSTE_TEXTO_RONDA_Y, paint);
        paint.setColor(Color.RED);
        paint.setTextSize(45);

    }


    public static void FINALIZAR_JUEGO() {
        HILO_JUEGO.perder();

    }

    public int getRonda() {
        return ronda;
    }

    public void setFondo(Background fondo) {
        this.fondo = fondo;
    }

    public void setFilas(Fila[] filas) {
        this.filas = filas;
    }

    public void setBotonGuitarra(Rectangle botonGuitarra) {
        this.botonGuitarra = botonGuitarra;
    }

    public void setZonaAliados(Rectangle zonaAliados) {
        this.zonaAliados = zonaAliados;
    }

    public void setBotonViolin(Rectangle botonViolin) {
        this.botonViolin = botonViolin;
    }

    public void setBotonSaxofon(Rectangle botonSaxofon) {
        this.botonSaxofon = botonSaxofon;
    }

    public void reiniciar()
    {
        puntuacion = 350;
        ronda = 0;
        ENEMIGOS_RESTANTES_RONDA = 0;
        for(Fila f : filas)
            f.vaciaFila();
        tiempoEntreEnemigos = 1.5f;
        this.HILO_JUEGO = new HiloJuegoPrincipal(this.getHolder(), this, paint);
        HILO_JUEGO.start();

    }

    public void volverMenuPrincipal() {
        ((ActividadJuego)this.getContext()).cerrarActividad();
    }
}
