package ampe.juego.EntidadesInmutables;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Iterator;
import java.util.LinkedList;

import ampe.juego.EntidadesFila.Aliado;
import ampe.juego.EntidadesFila.Enemigo;
import ampe.juego.EntidadesFila.Proyectil;

public class Fila {
    //Cual de las tres filas corresponde, la primera en el cuadrante 8-9,33
    //La segunda ene l cuadrante 9,33-10.66  y la tercera en el 10.66-11.99
    private int posicionFila;
    //Colección de los enemigos que están en la fila
    private LinkedList<Enemigo> enemigos;
    //Colección de los proyectiles que están en la fila
    private LinkedList<Proyectil> proyectiles;
    //Máximo de 5 posiciones para aliados
    private Aliado aliados[];
    private int ajusteEjeY;
    private int longitudEjeX;
    //Cambiar esto si se quieren aumentar las filas
    private final int NFILAS = 3;
    private final int NALIADOS = 5;
    //Las zonas entre la 8 y la 12 son dibujables
    private final int ZONAS_DIBUJABLES = 4;
    //Zona donde empieza a ser dibujable
    private final int PRINCIPIO_DIBUJABLE = 8;
    //Numero de zonas en las que se divide la imagen
    private final int ZONAS_TOTALES = 15;
    //Bitmap que se utilizará cuando se quiera saber que posiciones de una fila están disponibles
    private Bitmap simboloAdd;


    public Fila(int posicionFila,int longitudEjeX, int longitudEjeY, Bitmap add)
    {
        this.posicionFila = posicionFila;
        enemigos = new LinkedList<Enemigo>();
        proyectiles = new LinkedList<Proyectil>();
        aliados  = new Aliado[NALIADOS];
        //Ajuste de ejeY, dividimos la imagen en 15 zonas, y la zona dibujable está entre la 8 y la 12
        //Por tanto dependiendo de la fila y del alto del backgropund ajustamos,
        ajusteEjeY = (longitudEjeY  * (PRINCIPIO_DIBUJABLE + (posicionFila-1)*(ZONAS_DIBUJABLES/NFILAS)))/ZONAS_TOTALES;
        this.longitudEjeX = longitudEjeX;
        this.simboloAdd = add;
    }

    /**
     * Hacer un draw de todos los elementos de la Fila, en caso de que se esté intentando
     * introducir un nuevo Aliado también se hará Draw de las posiciones libres
     */
    public void draw(Canvas canvas, Paint paint)
    {
        for(Enemigo e : enemigos)
            e.draw(canvas, paint);
        for(Proyectil p : proyectiles)
            p.draw(canvas, paint);
        for(int x = 0; x<aliados.length;x++)
                if(aliados[x]!=null)
                    aliados[x].draw(canvas, paint);
                //Dibujar zonas libres para aliados
                else if(PanelJuego.COLOCANDO_ALIADO)
                {
                    if (canvas != null) {
                        final int instanciaGuardada = canvas.save();
                        canvas.drawBitmap(simboloAdd, (int)((longitudEjeX * (1 + (x  * (15.5/5))))/26.5), ajusteEjeY+50, paint);
                        canvas.restoreToCount(instanciaGuardada);
                    }
                }


    }
    /**
     * Método utilizado antes de actualizar las posiciones para saber que debe actualizarse
     * y que eliminarse
     */
    private void comprobarColisiones()
    {
        /**
         * Primero comprobamos colisiones entre disparos y enemigos y luego entre enemigos y
         * aliados
         */
        //Variable que indica si colisiona con un proyectil y por tanto se elimina
        //por lo que no habría que comprobar si choca con algún aliado
        boolean haSidoEliminado = false;
        for (Iterator<Enemigo> iteradorEnemigos = enemigos.iterator(); iteradorEnemigos.hasNext();) {
            Enemigo e = iteradorEnemigos.next();
            for (Iterator<Proyectil> iteradorProyectiles = proyectiles.iterator(); iteradorProyectiles.hasNext(); ) {
                Proyectil p = iteradorProyectiles.next();
                //Como solo depende de la X comprobamos si una está dentro de la otra, es decir, colisionan
                if (((e.getX() <= p.getX()) && (e.getX()+e.getAncho()) >= p.getX()) ||
                        ((p.getX()<= e.getX()) && ((p.getX() + p.getAncho()) >= e.getX()))) {
                    // Remove the current element from the iterator and the list.
                    iteradorEnemigos.remove();
                    //Queda uno menos para cambiar de ronda y se lo comunicamos al PanelJuego
                    PanelJuego.ENEMIGOS_RESTANTES_RONDA--;
                    iteradorProyectiles.remove();
                    haSidoEliminado = true;
                }
                else haSidoEliminado = false;


            }
            /**
             * Ahora que hemos comprobbado que no se ha dado con ningún proyectil podemos ver si
             * elimina a algún aliado
             */
            if(!haSidoEliminado)
                for(int i = 0; i<aliados.length; i++)
                    if(aliados[i] != null)
                        if (((e.getX() <= aliados[i].getX()) && (e.getX() + e.getAncho()) >= aliados[i].getX()) ||
                                ((aliados[i].getX() <= e.getX()) && ((aliados[i].getX() + aliados[i].getAncho()) >= e.getX())))
                            // Remove the current element from the iterator and the list.
                            aliados[i] = null;



        }
    }

    /**
     * Update de todo lo que hay en la fila,
     * se comprueban primero las colisiones para que se queden dibujadas y se vean
     * antes de borrar las imágenes
     */
    public void update()
    {
        comprobarColisiones();
        //Antes de actualizar se comprueban colisiones
        for(Enemigo e : enemigos)
            e.update();
        for (Iterator<Proyectil> iteradorProyectiles = proyectiles.iterator(); iteradorProyectiles.hasNext(); ) {
            Proyectil p = iteradorProyectiles.next();
            if (p.getX() >= this.longitudEjeX) {
                /** Eliminar el proyectil si se pasa de la longitud de la pantalla*/
                iteradorProyectiles.remove();
            }
            else
                p.update();
        }
        for(Aliado a : aliados)
            if(a!=null)
                a.update();
    }

    //Añade un enemigo a la Fila, lo pone al final y en el ejeY ajustado según la posición de la fila
    public void addEnemigo(Enemigo e)
    {
        e.setY(ajusteEjeY);
        enemigos.addLast(e);
    }

    /**
     *Añade un aliado a la fila en la posición pasada como parámetro
     * Y le pasa una referencia a la propia fila para que pueda crear los proyectiles
     * con la acción disparar
     */
    public void addAliado(Aliado a, int posicion)
    {
        a.setY(ajusteEjeY);
        a.setFilaActual(this);
        a.setX((int)((longitudEjeX * (1 + ((posicion-1)  * (15.5/5))))/26.5));
        aliados[posicion-1] = a ;
    }

    /**
     *
     * Método que utilizan los Aliados para crear y añadir los proyectiles, la posición x es la posición
     * x del aliado más el ancho, es decir, el borde del aliado
     */
    public void creaProyectil(int posicionInicioX) {
        Proyectil p = PanelJuego.FABRICA_OBJETOS_FILAS.crearProyectil();
        p.setX(posicionInicioX);
        p.setY(ajusteEjeY+50);
        this.proyectiles.add(p);

    }

    /**
     * Para saber si hay o no un aliado en esa posicion
     */
    public boolean isPosicionOcupada(int posicion) {
        return (aliados[posicion-1] !=null);
    }

    public void vaciaFila() {
        this.enemigos.clear();
        this.proyectiles.clear();
        for(int i = 0; i<NALIADOS;i++)
            this.aliados[i] = null;
    }
}
