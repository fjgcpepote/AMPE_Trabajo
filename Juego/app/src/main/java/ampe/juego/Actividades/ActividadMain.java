package ampe.juego.Actividades;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import ampe.juego.EntidadesInmutables.PanelJuego;
import ampe.juego.R;

public class ActividadMain extends AppCompatActivity {


    public static boolean CONTADOR_FPS = true;
    private Intent intentJuego;
    private Intent intentConfiguracion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Poner en pantalla completa
         */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.pantalla_inicio);
        final PanelJuego panel = PanelJuego.getUnicaInstancia(this);
        panel.cargarDatos();
        ActividadJuego.cancionFondo=MediaPlayer.create(ActividadMain.this, R.raw.background_theme);
        ActividadJuego.sonidoEnemigo=MediaPlayer.create(ActividadMain.this, R.raw.crash_bandicoot_woah);
        ActividadJuego.sonidoGuitarra=MediaPlayer.create(ActividadMain.this, R.raw.guitarra_sound);
        ActividadJuego.sonidoViolin=MediaPlayer.create(ActividadMain.this, R.raw.violin_sound);
        ActividadJuego.sonidoSaxofon=MediaPlayer.create(ActividadMain.this, R.raw.saxofon_sound);
        intentJuego = new Intent(this, ActividadJuego.class);
        intentConfiguracion = new Intent(this, ActividadConfiguracion.class);
        final Button bJugar = findViewById(R.id.botonJugar);
        final Button bConfigurar = findViewById(R.id.botonConfigurar);

        bJugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                panel.esperarDatos();
                startActivity(intentJuego);


            }
        });
        bConfigurar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentConfiguracion);

            }
        });



    }





}
