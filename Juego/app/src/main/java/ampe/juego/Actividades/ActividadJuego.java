package ampe.juego.Actividades;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import ampe.juego.EntidadesInmutables.PanelJuego;
import ampe.juego.R;


public class ActividadJuego extends Activity {
    static MediaPlayer cancionFondo;
    static MediaPlayer sonidoEnemigo;
    static MediaPlayer sonidoSaxofon;
    static MediaPlayer sonidoGuitarra;
    static MediaPlayer sonidoViolin;
    Button bSi = null;
    public static void playSonidoEnemigo(){
        if(!sonidoEnemigo.isPlaying())
            sonidoEnemigo.start();
    }

    public static void playSonidoSaxofon(){
        if(!sonidoSaxofon.isPlaying())
            sonidoSaxofon.start();
    }

    public static void playSonidoViolin(){
        if(!sonidoViolin.isPlaying())
            sonidoViolin.start();
    }

    public static void playSonidoGuitarra(){
        if(!sonidoGuitarra.isPlaying())
            sonidoGuitarra.start();
    }

    public static void playSonidoFondo()
    {
        if(!cancionFondo.isPlaying()) {
            cancionFondo.start();
            cancionFondo.setLooping(true);
        }
    }

    public static void liberarSonidos()
    {
        if(sonidoEnemigo.isPlaying())
            sonidoEnemigo.release();
        if(sonidoGuitarra.isPlaying())
            sonidoGuitarra.release();
        if(sonidoViolin.isPlaying())
            sonidoViolin.release();
        if(sonidoSaxofon.isPlaying())
            sonidoSaxofon.release();
        if(cancionFondo.isPlaying())
            cancionFondo.release();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       playSonidoFondo();
        /**
         * Poner en pantalla completa
         */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();

        PanelJuego panelJuego = PanelJuego.getUnicaInstancia(this);
        setContentView(panelJuego);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }
    @Override
    public void onBackPressed()
    {
        //No hacer nada
    }
    /**
     * Se hace release de los sonidos por que si no cuesta mucho salir de la aplicacion
     */
    @Override
    protected void onPause(){
        super.onPause();
        ActividadJuego.liberarSonidos();
    }

    public void cerrarActividad(){
       /** Intent intentMain = new Intent(this, ActividadMain.class);
        startActivity(intentMain);*/
        this.finish();
    };


}
