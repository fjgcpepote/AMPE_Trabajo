package ampe.juego.Controladores;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;

import ampe.juego.Actividades.ActividadJuego;
import ampe.juego.Actividades.ActividadMain;
import ampe.juego.EntidadesInmutables.PanelJuego;


public class HiloJuegoPrincipal extends Thread {
    //La cantidad de Frame máxima/deseada a la que irá el juego
    private int FPS = 30;
    //Los FPS medios a los que de verdad va el juego
    private double averageFPS;
    private SurfaceHolder manipuladorPantalla;//Manipulador pixeles...., eventos...
    //El panel de juego que se administra
    private PanelJuego panelJuego;
    //Si el juego sigue ejecutándose
    private boolean enEjecucion=false;
    private boolean salir = false;
    public static Canvas canvasDibujar;//Tien las rutinas Draw
    private Paint paint; //Para añadir texto y un pintado? común
    //Si hemos perdido la partida o aún no
    private boolean perdido = false;
    public void  empezarJuego()
    {
        this.enEjecucion = true;
    }
    public void finalizarJuego()
    {
        this.enEjecucion = false;
    }
    public void perder()
    {
        this.enEjecucion = false;
        ActividadJuego.liberarSonidos();
        perdido = true;
    }

    /**
     *Creación del hilo principal, aunque parezca un manipulador de pantalla distinto es el mismo
     * al del panelJuego
     */
    public HiloJuegoPrincipal(SurfaceHolder manipuladorPantalla, PanelJuego panelJuego, Paint paint)

    {

        this.manipuladorPantalla = manipuladorPantalla;
        this.panelJuego = panelJuego;
        this.paint = paint;

    }
    @Override
    public void run()
    {
        empezarJuego();
        //Tiempo antes de calcular y dibujar el nuevo frame
        long tiempoInicio;
        //Variable auxiliar para el tiempo en milésimas
        long timeMillis;
        //Tiempo que habría que esperar si vamos más rápido de FPS
        long waitTime;
        //Tiempo que ha transcurrido hasta tener todos los FPS
        long totalTime = 0;
        //Numero de Frames que ha dado tiempo a pintar
        int frameCount = 0;
        //El tiempo que quiero tener para conseguir x FPS
        long targetTime = 1000/FPS;

            while (enEjecucion) {
                tiempoInicio = System.nanoTime();
                canvasDibujar = null;
                try {
                    canvasDibujar = this.manipuladorPantalla.lockCanvas();
                    synchronized (manipuladorPantalla) {
                        //Esto actualiza todas las entidades dentro de Panel de juego
                        this.panelJuego.update();
                        //Esto las dibuja
                        this.panelJuego.draw(canvasDibujar);
                        frameCount++;
                        if(ActividadMain.CONTADOR_FPS)
                            canvasDibujar.drawText("FPS: " + averageFPS, 100, 50, paint);
                        /**
                         * Si un enemigo ha llegado a nuestra esquina, hemos perdido y salimos de la aplicación
                         */

                        if (perdido) {
                            paint.setTextSize(50);

                            canvasDibujar.drawText("Has perdido en la ronda:  " + panelJuego.getRonda(), panelJuego.getWidth() * 0.4f, panelJuego.getHeight() * 0.4f, paint);
                            paint.setTextSize(45);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (canvasDibujar != null) {
                        try {
                            manipuladorPantalla.unlockCanvasAndPost(canvasDibujar);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                timeMillis = (System.nanoTime() - tiempoInicio) / 1000000;
                waitTime = targetTime - timeMillis;
                if (waitTime > 0)
                    try {

                        this.sleep(waitTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                totalTime += System.nanoTime() - tiempoInicio;
                if (frameCount == FPS) {
                    averageFPS = 1000 / ((totalTime / frameCount) / 1000000);
                    frameCount = 0;
                    totalTime = 0;


                }

            }


        try {
            this.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //  panelJuego.reiniciar();
        panelJuego.volverMenuPrincipal();


    }

}
