package ampe.juego.Controladores;


import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.DisplayMetrics;

import ampe.juego.EntidadesInmutables.Background;
import ampe.juego.EntidadesInmutables.Fila;
import ampe.juego.EntidadesInmutables.PanelJuego;
import ampe.juego.Fabricas.FabricaAliados;
import ampe.juego.Fabricas.FabricaEnemigos;
import ampe.juego.Fabricas.FabricaObjetosFilas;
import ampe.juego.R;

public class HiloCargador extends Thread {
    private PanelJuego panelJuego;
    public HiloCargador(PanelJuego panelJuego)
    {
        this.panelJuego = panelJuego;
    }

    public void run()
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity)panelJuego.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int altoPantalla = displayMetrics.heightPixels;
        int anchoPantalla = displayMetrics.widthPixels;
        Resources recursos = panelJuego.getResources();
        Background fondo = new Background(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(recursos, R.drawable.background_graveyard),anchoPantalla, altoPantalla, true));
        panelJuego.setFondo(fondo);
        //Los plus para saber si se puede poner o no un aliado en una zona
        Bitmap add = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(recursos, R.drawable.blue_plus_icon_6), anchoPantalla/ 25, altoPantalla / 25, true);

        PanelJuego.FABRICA_ENEMIGOS = new FabricaEnemigos(panelJuego.getContext(), anchoPantalla, altoPantalla);
        PanelJuego.FABRICA_OBJETOS_FILAS = new FabricaObjetosFilas(panelJuego.getContext(), anchoPantalla, altoPantalla);
        PanelJuego.FABRICA_ALIADOS = new FabricaAliados(panelJuego.getContext(), anchoPantalla, altoPantalla);
        Fila[] filas = new Fila[PanelJuego.NFILAS];
        for (int i = 0; i < PanelJuego.NFILAS; i++)
        {
            filas[i] = new Fila(i+1,anchoPantalla, altoPantalla, add);
        }
        panelJuego.setFilas(filas);
        Rectangle botonGuitarra = new Rectangle();
        Rectangle botonViolin = new Rectangle();
        Rectangle botonSaxofon = new Rectangle();
        Rectangle zonaAliados = new Rectangle();
        //Ajustes para saber si se pulsan o no los botones
        int xGuitarra = (int)(PanelJuego.PULGADAS_ANCHO_BOTON *anchoPantalla / PanelJuego.PULGADAS_ANCHO_FONDO);
        int anchoRectangulos =  (int)(PanelJuego.PULGADAS_ANCHO_BOTON * anchoPantalla / PanelJuego.PULGADAS_ANCHO_FONDO);
        int altoRectangulos = (int)(PanelJuego.PULGADAS_ALTO_BOTON * altoPantalla / PanelJuego.PULGADAS_ALTO_FONDO);
        botonGuitarra.setBounds(xGuitarra, 0,anchoRectangulos, altoRectangulos);
        botonViolin.setBounds(xGuitarra + anchoRectangulos,0,anchoRectangulos, altoRectangulos);
        botonSaxofon.setBounds(xGuitarra+anchoRectangulos*2, 0,anchoRectangulos, altoRectangulos);
        zonaAliados.setBounds(0,
                (int)((altoPantalla*PanelJuego.PULGADA_Y_INICIO_ZONA_ALIADOS)/PanelJuego.PULGADAS_ALTO_FONDO), //y
                (int) ((anchoPantalla/PanelJuego.PULGADAS_ANCHO_FONDO)*PanelJuego.PULGADA_X_FIN_ZONA_ALIADOS), //La pulgada donde acaba, como x = 0 es lo mismo que el ancho
                (int)((altoPantalla/PanelJuego.PULGADAS_ALTO_FONDO)*PanelJuego.PULGADAS_ALTO_ZONA_ALIADOS));
        panelJuego.setBotonGuitarra(botonGuitarra);
        panelJuego.setBotonViolin(botonViolin);
        panelJuego.setBotonSaxofon(botonSaxofon);
        panelJuego.setZonaAliados(zonaAliados);
    }
}
