package ampe.juego.EntidadesFila;

import android.graphics.Bitmap;
import java.util.LinkedList;


public class Proyectil extends EntidadFila {

    private int largoPantalla;

    public Proyectil(LinkedList<Bitmap> sprites, int fila, int anchoProyectil, int largoPantalla) {
        super(sprites, 0, fila*AJUSTE, anchoProyectil, 10);
        this.largoPantalla = largoPantalla;
        this.sprites = sprites;


    }
    @Override
    public void update() {
        if(x<largoPantalla)
         this.x +=this.aceleracionX;
    }
}
