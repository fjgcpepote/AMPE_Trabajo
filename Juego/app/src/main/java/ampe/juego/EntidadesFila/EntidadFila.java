package ampe.juego.EntidadesFila;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.LinkedList;

/**
 * Se crean Entidades que estarán cada una en una de las X filas
 */

public abstract class  EntidadFila {
    //Imagen que se dibuja
    protected Bitmap spriteActual;
    //Coordenadas x = posX, y = fila
    protected int x, y;
    protected int ancho;
    //Velocidad a la que se puede mover la entidad
    protected int aceleracionX;
    //Para colocar en la pantalla
    protected static  final int AJUSTE = 250;
    //Los dibujos que llevaran
    protected LinkedList<Bitmap> sprites;
    //Llamar con super()
    protected EntidadFila(LinkedList<Bitmap> sprites, int x, int y,int ancho, int aceleracionX)
    {
        this.sprites = sprites;
        this.x = x;//Casi siempre sobreescrito
        this.y = y;//Casi siempre sobreescrito
        this.aceleracionX = aceleracionX;
        this.spriteActual = sprites.getFirst();
        this.ancho = ancho;
    }

    /**
     *Todos los herederos de EntidadFila se dibujan igual
     */
    public void draw(Canvas canvas, Paint paint) {

        if (canvas != null) {
            //Esta es la forma eficiente de hacerse
            final int instanciaGuardada = canvas.save();
            canvas.drawBitmap(spriteActual, x, y, paint);
            canvas.restoreToCount(instanciaGuardada);
        }


    }

    /**
     * Cada heredero de EntidadFila debe sobreescribir update
     */
    public abstract void update();

    public Bitmap getSpriteActual() {
        return spriteActual;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }



}
