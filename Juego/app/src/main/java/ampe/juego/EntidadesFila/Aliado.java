package ampe.juego.EntidadesFila;


import android.graphics.Bitmap;

import java.util.LinkedList;

import ampe.juego.EntidadesInmutables.Fila;

public class Aliado extends EntidadFila {

    //Contador de disparos, utilizado por los aliados como una cuenta para saber cuando disparar
    protected int contadorDisparos;
    //Los aliados tienen una referencia a su fila para poder meter los proyectiles que crean
    //en esa fila
    protected Fila filaActual;
    //Disparar, método que hay que sobbreescribir en los hijos para que haga algo
    //Ese algo será poner x balas en la fila
    private void disparar()
    {

    };
    //Como cualquier entidad fila, hay que pasarle la lista de sprites que utilizará,
    //el ancho del aliado para comprobar colisiones, y PosicionX que no será usado ya que
    //la Posicion X la decidirá la Fila cuando aliado se agregue a ella
    protected Aliado(LinkedList<Bitmap> sprites, int anchoAliado, int posicionX) {
        super(sprites, posicionX, 0, anchoAliado,0);
        this.sprites = sprites;
        //30 para que no tarde tanto en empezar a disparar
        contadorDisparos = 30;


    }
    @Override
    public void update() {
        //Se encargarán los hijos
    }
    //Metodo para añadir la Fila a la que pasará a pertenecer el Aliado y por tanto donde
    //colocará las bbalas
    public void setFilaActual(Fila filaActual) {
        this.filaActual = filaActual;
    }
}
