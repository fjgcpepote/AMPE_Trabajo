package ampe.juego.EntidadesFila;

import android.graphics.Bitmap;

import java.util.LinkedList;

import ampe.juego.Actividades.ActividadJuego;


public class Violin extends Aliado {
    public Violin(LinkedList<Bitmap> sprites, int anchoAliado, int posicionX) {
        super(sprites, anchoAliado, posicionX);
        this.filaActual = null;
        ActividadJuego.playSonidoViolin();
    }
    private void disparar()
    {
        if(filaActual!=null)
            filaActual.creaProyectil(this.getX() + ancho);
    }
    @Override
    public void update()
    {
        super.update();
        this.contadorDisparos++;
        //Violin más rápido que guitarra
        if(contadorDisparos >= 50)
        {
            disparar();
            contadorDisparos = 0;
        }
    }
}
