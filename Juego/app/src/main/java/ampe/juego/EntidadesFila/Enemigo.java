package ampe.juego.EntidadesFila;

import android.graphics.Bitmap;
import java.util.LinkedList;

import ampe.juego.Actividades.ActividadJuego;
import ampe.juego.EntidadesInmutables.PanelJuego;


public class Enemigo extends EntidadFila {

    //Indice que indica que sprite se está dibujando en este momento, se empezará por el primero
    //Y luego se irá en una lista circular
    private int indice = 0;

    /**
     *Contructor como cuqluier EntidadFila, salvo que es necesario el largo de la pantalla
     * ya que  los enemigos irán de derecha a izquierda y la pantalla puede cambiar
     */
    public Enemigo( LinkedList<Bitmap> sprites, int anchoEnemigo, int largoPantalla) {
        //Aceleración negativa para ir a la izquierda
        //y = 0 porque la fila donde se meta ya decidirá su y
        super(sprites, largoPantalla+30, 0, anchoEnemigo,-2);
        this.sprites = sprites;
        //Esto activa el reproductor de sonidos
        ActividadJuego.playSonidoEnemigo();

    }


    public void update()
    {
        /**
         * Si un enemigo llega al borde izquierdo has perdido
         */
        if(x<=0)
            PanelJuego.FINALIZAR_JUEGO();
        else {
            //El enemigo se mueve dependiendo de la aceleración
            this.x += this.aceleracionX;

            indice++;
            indice = indice % this.sprites.size();
            //Cambia de sprite y en caso de que llegue al final vuelve al primero

            this.spriteActual = this.sprites.get(indice);
        }



    }

    /**
     *
     * Método utilizado para que cada ronda los enemigos sean más rápidos
     */
    public void aumentarAceleracion(int ronda)
    {
        this.aceleracionX-=ronda;
    }


}



