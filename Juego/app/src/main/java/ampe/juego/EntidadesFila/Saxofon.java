package ampe.juego.EntidadesFila;


import android.graphics.Bitmap;

import java.util.LinkedList;

import ampe.juego.Actividades.ActividadJuego;

public class Saxofon extends Aliado {

    public Saxofon(LinkedList<Bitmap> sprites, int anchoAliado, int posicionX) {
        super(sprites, anchoAliado, posicionX);
        this.filaActual = null;
        ActividadJuego.playSonidoSaxofon();
    }
    private void disparar()
    {
        if(filaActual!=null)
         filaActual.creaProyectil(this.getX() + ancho);
    }
    @Override
    public void update()
    {
        super.update();
        this.contadorDisparos++;
        //Saxofon es el más lento pero dispara tres veces seguidas
        if(contadorDisparos == 250 || contadorDisparos == 255 || contadorDisparos == 260)
        {
            disparar();
            if(contadorDisparos == 260)
                contadorDisparos = 0;

        }

    }
}
