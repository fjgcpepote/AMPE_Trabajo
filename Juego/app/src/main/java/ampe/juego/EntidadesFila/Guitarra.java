package ampe.juego.EntidadesFila;


import android.graphics.Bitmap;

import java.util.LinkedList;

import ampe.juego.Actividades.ActividadJuego;

public class Guitarra extends Aliado {
    public Guitarra(LinkedList<Bitmap> sprites, int anchoAliado, int posicionX) {
        super(sprites, anchoAliado, posicionX);
        this.filaActual = null;
        ActividadJuego.playSonidoGuitarra();
    }
    private void disparar()
    {
        if(filaActual!=null)
            filaActual.creaProyectil(this.getX() + ancho);
    }
    @Override
    public void update()
    {
        super.update();
        this.contadorDisparos++;
        //Guitara buen coste/disparo
        if(contadorDisparos == 90)
        {
            disparar();
            contadorDisparos = 0;
        }
    }
}
