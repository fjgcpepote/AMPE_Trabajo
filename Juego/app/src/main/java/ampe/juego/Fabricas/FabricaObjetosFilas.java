package ampe.juego.Fabricas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.LinkedList;

import ampe.juego.EntidadesFila.Proyectil;
import ampe.juego.R;

/**
 * Fabrica de proyectiles, aunque se llama así por si se introducían otro tipo de objetos
 */
public class FabricaObjetosFilas {

    /**
     * Contexto para los recursos
     */
    private Context contexto;
    //Imágenes de los proyectiles
    private LinkedList<Bitmap> sprites;
    private int anchoPantalla, altoPantalla;
    private  int ajusteAnchoProyectil = 20;
    private  int ajusteAltoProyectil = 15;
    public FabricaObjetosFilas(Context contexto, int anchoPantalla, int altoPantalla) {
        this.contexto = contexto;
        this.altoPantalla = altoPantalla;
        this.anchoPantalla = anchoPantalla;
        this.sprites = new LinkedList<Bitmap>();
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.proyectil), anchoPantalla / ajusteAnchoProyectil, altoPantalla / ajusteAltoProyectil, true));
    }

    public Proyectil crearProyectil()
    {
        return new Proyectil(sprites,0,anchoPantalla / ajusteAnchoProyectil, anchoPantalla);
    }

}
