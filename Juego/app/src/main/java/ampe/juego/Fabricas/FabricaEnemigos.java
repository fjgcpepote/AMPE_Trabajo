package ampe.juego.Fabricas;

import android.graphics.Bitmap;


import android.content.Context;
import android.graphics.BitmapFactory;

import java.util.LinkedList;

import ampe.juego.EntidadesFila.Enemigo;
import ampe.juego.R;

public class FabricaEnemigos {
    /**
     * Contexto para los recursos
     */
    private Context contexto;
    //Sprites del Enemigo
    private LinkedList<Bitmap> sprites;
    private int ancho, alto;
    private final int AJUSTAR_ENEMIGOX = 15;
    private final int AJUSTAR_ENEMIGOY = 10;
    public FabricaEnemigos(Context contexto, int ancho, int alto) {
        this.contexto = contexto;
        this.alto = alto;
        this.ancho = ancho;
        this.sprites = new LinkedList<Bitmap>();
        /**
         * Carga en memoria de todos los sprites que va a tener el enemigo
         */
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite1), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite2), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite3), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite4), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite5), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite6), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite7), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite8), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite9), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite10), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));


        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite11), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite12), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite13), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite14), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite15), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite16), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite17), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite18), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite19), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
        this.sprites.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.sprite20), ancho / AJUSTAR_ENEMIGOX, alto / AJUSTAR_ENEMIGOY, true));
    }

    public Enemigo creaEnemigo()
    {
        return new Enemigo(this.sprites,ancho/AJUSTAR_ENEMIGOX,ancho );
    }
}
