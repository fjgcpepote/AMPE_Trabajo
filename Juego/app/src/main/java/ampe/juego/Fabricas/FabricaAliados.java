package ampe.juego.Fabricas;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.LinkedList;

import ampe.juego.Enum.AliadoID;
import ampe.juego.EntidadesFila.Aliado;
import ampe.juego.EntidadesFila.Guitarra;
import ampe.juego.EntidadesFila.Saxofon;
import ampe.juego.EntidadesFila.Violin;
import ampe.juego.R;

/**
 * Clase utilizada para reducir la complejidad de crear Aliados y no pasar muchas veces el contexto
 * mala praxis
 */
public class FabricaAliados {
    //De donde se van a sacar las imágenes para obtener el BitMap
    private Context contexto;
    //Sprites del Aliado Saxofon
    private LinkedList<Bitmap> spritesSaxofon;
    //Sprites del Aliado Guitarra
    private LinkedList<Bitmap> spritesGuitarra;
    //Sprites del Aliado Violin
    private LinkedList<Bitmap> spritesViolin;
    //El ancho y el alto de la pantalla para poder ajustar las coordenadas x e y
    private int anchoPantalla, altoPantalla;
    //Ajuste para mantener una buena relación de aspecto en todos los Aliados, dibujándose en forma de rectángulo
    private  int ajusteAnchoAliado = 14;
    private  int ajusteAltoAliado = 7;

    public FabricaAliados(Context contexto, int anchoPantalla, int altoPantalla) {
        this.contexto = contexto;
        this.altoPantalla = altoPantalla;
        this.anchoPantalla = anchoPantalla;
        this.spritesSaxofon = new LinkedList<Bitmap>();
        this.spritesViolin = new LinkedList<Bitmap>();
        this.spritesGuitarra = new LinkedList<Bitmap>();
        this.spritesSaxofon.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.saxofon), anchoPantalla / ajusteAnchoAliado, altoPantalla / ajusteAltoAliado, true));
        this.spritesGuitarra.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.guitarra), anchoPantalla / ajusteAnchoAliado, altoPantalla / ajusteAltoAliado, true));
        this.spritesViolin.addLast(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(contexto.getResources(), R.drawable.violin), anchoPantalla / ajusteAnchoAliado, altoPantalla / ajusteAltoAliado, true));

    }

    /**
     *
     * Funciones para crear Aliados Concretos
     */
    public Saxofon crearSaxofon()
    {
        return new Saxofon(spritesSaxofon,anchoPantalla / ajusteAnchoAliado, 0);
    }
    public Guitarra crearGuitarra()
    {
        return new Guitarra(spritesGuitarra,anchoPantalla / ajusteAnchoAliado, 0);
    }

    public Violin crearViolin()
    {
        return new Violin(spritesViolin,anchoPantalla / ajusteAnchoAliado, 0);
    }

    /**
     *
     *Funcion para crear un Aliado Concreto a partir de un ID
     */
    public Aliado crearAliadoID(AliadoID id) {
        switch(id)
        {
            case GUITARRA: return crearGuitarra();
            case VIOLIN: return crearViolin();
            case SAXOFON: return crearSaxofon();

        }
        return null;
    }
}
